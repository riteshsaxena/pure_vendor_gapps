# Copyright (C) 2017 The Pure Nexus Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#inherit common gapps
$(call inherit-product, vendor/gapps/common-blobs.mk)

# /app
PRODUCT_PACKAGES += \
    CalendarGooglePrebuilt \
    PrebuiltDeskClockGoogle \
    PrebuiltBugle

# /framework
PRODUCT_PACKAGES += \
    com.google.android.camera.experimental2016 \
    com.google.android.maps \
    com.google.android.media.effects

#Widivine framework - pixels dont use
ifeq ($(filter marlin sailfish,$(TARGET_PRODUCT)),)
PRODUCT_PACKAGES += \
    com.google.widevine.software.drm
endif

# /symlinks
PRODUCT_PACKAGES += \
    libfacenet.so
